# Vue 示例工程

* 基于`vue`+`vuex`+`vue-router`+`element-ui`

## 目录结构说明
```html
src
├──assets           // 图片等静态资源
├──common              // 通用工具
│   ├── conf       // 配置相关
│   │    ├── index    // 主入口
│   │    └── constant    // 常量配置
│   └── utils       // 统一工具类入口，作为所有依赖引用的统一入口  
├──css                  // less文件
│   ├── login       // 登陆
│   ├── ...         // 每个组件/页面一个目录
│   └── main        // 整合样式文件
├──store            // store
│   ├── store.js    // 整合store
│   ├── user.js     // 用户store    
│   └── ...         // 其他模块store
├──components       //　组件
│   ├── layout      // 布局组件
│   ├── login       // 登陆
│   └── ...         //每个目录一个组件
├──mock            // 存放mock文件
├──main.js           // 加载主文件
└──routes.js        // 路由控制
```
## 关于项目名

默认项目名为空，如果需要加上项目名需要修改如下位置：
- build/project.config.js `applicationContext`变量
- src/common/conf/index.js `baseUrl`变量
- src/common/css/main.less `baseUrl`变量

## 安装

```bash
npm install
```

## 开发

```bash
gulp
```
地址栏打开地址 `http://localhost:8010`

## 发布
```bash
gulp build
```
将`dist`目录中代码复制到项目目录


> 注意事项
1. 使用less书写样式
2. 采用es6语法
3. 基于eslint有语法校验
4. 所有个人开发分支需要合并到`develop`分支，指定`chizongyang`进行合并