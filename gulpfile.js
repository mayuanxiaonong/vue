const gulp = require('gulp')
const del = require('del')
const changed = require('gulp-changed')
// css
const less = require('gulp-less')
const sourceMaps = require('gulp-sourcemaps')
// const picbase64 = require('gulp-base64')
// image
// const minimage = require('gulp-imagemin')
// js
const webpack = require('webpack')

const webpackConfig = require('./build/webpack.config')
const webpackstream = require('webpack-stream')
const chmod = require('gulp-chmod')

const browserSync = require('browser-sync').create()
const proxy = require('http-proxy-middleware')

const { reload } = browserSync
const config = require('./build/project.config')

gulp.task('copyPrecommit', () => {
    gulp.src(config.precommitSrc)
        .pipe(chmod({
            owner: {
                read: true,
                write: true,
                execute: true
            },
            group: {
                execute: true
            },
            others: {
                execute: true
            }
        }))
        .pipe(gulp.dest(config.precommitDest))
})

gulp.task('styles', () => {
    gulp.src(config.lessMain)
        .pipe(sourceMaps.init())
        .pipe(changed(config.lessDest))
        .pipe(less({
            paths: [config.less]
        }))
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(config.lessDest))
        .pipe(reload({ stream: true }))
})

gulp.task('styles_build', () => {
    gulp.src(config.lessMain)
        .pipe(less({
            paths: [config.less]
        }))
        .pipe(gulp.dest(config.lessDest))
})

gulp.task('clean', () => del(['dist']).then(() => {
    console.log('删除完成')
}))

gulp.task('images', () => {
    gulp.src(config.images)
        .pipe(gulp.dest(config.destImg))
})

gulp.task('copyFonts', () => {
    gulp.src(config.fonts).pipe(gulp.dest(config.destFonts))
})

gulp.task('copyMonaco', () => {
    return gulp.src('./node_modules/monaco-editor/min/**/*')
        .pipe(gulp.dest(config.distPrefix))
})

gulp.task('copy', () => {
    gulp.start(['copyFonts', /* 'copyPrecommit', */ 'copyMonaco'])
})

gulp.task('eslint', () => {
    const myWebpackConfig = Object.assign({}, webpackConfig)
    myWebpackConfig.watch = false
    myWebpackConfig.entry.app.splice(0, 1)
    return gulp.src(config.mainJS)
        .pipe(webpackstream(myWebpackConfig))
        .pipe(gulp.dest(config.dist))
})

gulp.task('mock', () => {
    return gulp.src(config.mockSrc)
        .pipe(gulp.dest(config.mockDest))
})

gulp.task('webpack', () => {
    const myWebpackConfig = Object.assign({}, webpackConfig)
    // delete myWebpackConfig.output
    // delete myWebpackConfig.entry
    myWebpackConfig.entry.app.splice(0, 1)

    return gulp.src('src/main.js')
        .pipe(webpackstream(myWebpackConfig))
        .pipe(gulp.dest(config.distPrefix))
        .pipe(reload({ stream: true }))
})

gulp.task('webpack_build', () => {
    const myWebpackConfig = Object.assign({}, webpackConfig)
    const newApp = webpackConfig.entry.app[2]
    delete myWebpackConfig.devServer
    myWebpackConfig.watch = false
    myWebpackConfig.devtool = 'cheap-module-source-map'
    myWebpackConfig.entry.app = newApp

    myWebpackConfig.plugins.shift()
    myWebpackConfig.plugins.shift()
    myWebpackConfig.plugins.shift()
    myWebpackConfig.plugins.unshift(new webpack.optimize.ModuleConcatenationPlugin())
    myWebpackConfig.plugins.unshift(new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
    }))
    myWebpackConfig.plugins.unshift(new webpack.optimize.UglifyJsPlugin(config.uglifyJsConfig))

    delete myWebpackConfig.output.sourceMapFilename
    
    myWebpackConfig.output.publicPath = `${config.applicationContext}`

    return gulp.src(config.mainJS)
        .pipe(webpackstream(myWebpackConfig))
        .pipe(gulp.dest(config.distPrefix))
})

gulp.task('browserSync', () => {
    const { paths, target, proxyPort } = config.proxy
    const filter = function (pathname){
        return (pathname.match(paths.map(item => (`^${item}`)).join('|')))
    }
    const middleware = proxy(filter, {
        target: `${target}:${proxyPort}`,
        changeOrigin: true
    })

    browserSync.init({
        middleware: [middleware],
        server: {
            baseDir: './dist'
        },
        host: config.ip,
        port: config.port,
        open: false
    })
    gulp.watch(config.less, ['styles'])
    gulp.watch(config.images, ['images'])
})

gulp.task('build', ['clean'], () => {
    const arr = ['copyMonaco', 'copyFonts', 'webpack_build', 'styles_build', 'images']
    gulp.start(arr)
})

gulp.task('watch', ['clean'], () => {
    gulp.start(['copy', 'browserSync', 'mock', 'webpack', 'styles', 'images'])
})

gulp.task('default', ['clean'], () => {
    gulp.start(['watch'])
})
