module.exports = {
    'root': true,
    'extends': [
        'airbnb-base/legacy',
        'plugin:vue/recommended'
    ],
    // 'parser': 'babel-eslint',
    'parserOptions': {
        'parser': 'babel-eslint',
        'sourceType': 'module',
        'allowImportExportEverywhere': true,
        'ecmaVersion': 6
    },
    'env': {
        'browser': true,
        'node': true,
        'es6': true,
        'embertest': true
    },
    'settings': {
        'import/resolver': {
            'webpack': './build/webpack.config.js'
        }
    },
    'plugins': [
        'vue'
    ],
    'globals': {
        'domain': true,
        'location': true
    },
    'rules': {
        // 句尾分号可以省略
        'semi': [2, 'never'],
        // 代码中console/debugger处理
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': 'error',
        // 代码使用4个空格的缩进风格
        'indent': ['error', 4],
        // 关闭命名function表达式规则
        'func-names': 'off',
        // 关闭不允许修改参数（vue state可以直接修改）
        'no-param-reassign': 'off',
        // 可以行尾空白
        'no-trailing-spaces': 'off',
        // 关闭拖尾逗号
        'comma-dangle': 'off',
        // 关闭换行符转换
        'linebreak-style': 'off',
        // 禁止多余分号
        'no-extra-semi': 2,
        // 禁止使用指定语法
        'no-restricted-syntax': ['error', 'WithStatement'],
        // 关闭语句块之前的空格保持一致
        'space-before-blocks': 'off',
        // 可以使用++/--
        'no-plusplus': 'off',
        // 禁止未使用过的变量包括全局变量和函数中的最后一个参数必须使用
        'no-unused-vars': [
            'error', {
                'vars': 'all',
                'args': 'after-used'
            }
        ],
        'no-unused-expressions': ['error', {
            'allowTernary': true
        }],
        // 使用单引号
        'quotes': [
            'error', 'single'
        ],
        // 强制最大可嵌深度为3
        'max-depth': [
            'error', 3
        ],
        // 强制函数块中的语句最大50行
        'max-statements': [
            'error', 50
        ],
        // 强制行的最大长度150,注释200
        'max-len': [
            'error', {
                'code': 150,
                'comments': 200
            }
        ],

        // NodeJs rules， 9.0之后全部使用import
        // 关闭require()强制在模块顶部调用
        'global-require': 'off',
        
        // ES6 rules
        // 箭头函数的箭头前后都要有空格
        'arrow-spacing': 'error',
        // 接收const被修改的通知
        'no-const-assign': 'error',
        // 要求使用let或const而不是var
        'no-var': 'error',
        // 如果一个变量不会被重新赋值，则使用const声明
        'prefer-const': 'error',
        // 链接地址中不可以使用 javascript:;
        'no-script-url': 'off',        
        // vue缩进控制
        'vue/script-indent': ['error', 4],
        'vue/html-indent': ['error', 4],
        'vue/max-attributes-per-line': [2, {
            'singleline': 4,
            'multiline': {
                'max': 1,
                'allowFirstLine': true
            }
        }],
        'vue/html-self-closing': ['error', {
            'html': {
                'void': 'always',
                'normal': 'always',
                'component': 'always'
            },
            'svg': 'always',
            'math': 'always'
        }],
        // 关闭点击元素上强制增加onKey**事件
        'click-events-have-key-events': 'off',
        // 关闭引用依赖检查
        'import/no-extraneous-dependencies': 'off',
        // 不检查import引用
        'import/no-unresolved': 'off',
        'object-curly-newline': ['off', {
            'ObjectExpression': { 'multiline': true },
            'ObjectPattern': { 'multiline': true },
            'ImportDeclaration': { 'multiline': true },
            'ExportDeclaration': { 'multiline': true, 'minProperties': 3 }
        }],
        'no-new': 0
    }
}
