import VueRouter from 'vue-router'
import container from './components/layout/container.vue'
import basicInformation from './components/basicInformation/basicInformation.vue'
import NotFound from './components/404.vue'
import login from './components/login/login.vue'


// 交管所系统
import carCollection from './components/motorVehicleFile/fileCollection.vue'// 机动车档案采取
import carSupplement from './components/motorVehicleFile/fileSupplement.vue'// 机动车档案补录
import carList from './components/motorVehicleFile/fileList.vue' // 机动车档案列表
import carStatistics from './components/motorVehicleFile/statisticsFile.vue' // 机动车档案统计页面


import diverCollection from './components/driverFile/fileCollection.vue' // 驾驶人档案采取
import diverSupplement from './components/driverFile/fileSupplement.vue'// 驾驶人档案补录
import diverList from './components/driverFile/fileList.vue' // 驾驶人档案列表
import diverStatistics from './components/driverFile/statisticsFile.vue' // 驶人档案统计页面

import accidentCollention from './components/accidentDocumentFile/fileCollection.vue' // 事故文书档案采取
import accidentSupplement from './components/accidentDocumentFile/fileSupplement.vue' // 事故文书档案补录
import accidentList from './components/accidentDocumentFile/fileList.vue' // 事故文书档案列表
import accidentStatistics from './components/accidentDocumentFile/statisticsFile.vue' // 事故文书统计页面

import illegalCollection from './components/trafficViolationFile/fileCollection.vue' // 交通违章档案采取
import illegalSupplement from './components/trafficViolationFile/fileSupplement.vue' // 交通违章档案补录
import illegalList from './components/trafficViolationFile/fileList.vue' // 交通违章档案列表
import illegalStatistics from './components/trafficViolationFile/statisticsFile.vue' // 事故文书统计页面

import assessmentList from './components/checkFile/reviewTaskList.vue' // 考核任务列表
import assessmentStatistics from './components/checkFile/taskStatistics.vue' // 考核任务统计

import userManagement from './components/systemManagement/user.vue'// 用户管理
import roleManagement from './components/systemManagement/role.vue'// 角色管理
import departmentManagement from './components/systemManagement/department.vue'// 部门管理

import logManagement from './components/logManagement/logManagement.vue'// 日志管理

import parameterManagement from './components/parameterConfiguration/parameterConfiguration.vue' // 参数管理

const routes = [
    {
        path: '/',
        redirect: '/login'
    }, {
        name: 'login', 
        path: '/login',
        component: login
    }, {
        path: '/index',
        name: 'container',
        component: container,
        children: [
            {
                path: '/index',
                name: 'index',
                components: {
                    default: carCollection,
                } 
            }, {
                path: '/carCollection',
                name: 'carCollection',
                component: carCollection,
            }, {
                path: '/carSupplement',
                name: 'carSupplement',
                component: carSupplement,
            }, {
                path: '/carList',
                name: 'carList',
                component: carList,
            }, {
                path: '/carStatistics',
                name: 'carStatistics',
                component: carStatistics,
            }, {
                path: '/diverCollection',
                name: 'diverCollection',
                component: diverCollection,
            }, {
                path: '/diverSupplement',
                name: 'diverSupplement',
                component: diverSupplement,
            }, {
                path: '/diverList',
                name: 'diverList',
                component: diverList,
            }, {
                path: '/diverStatistics',
                name: 'diverStatistics',
                component: diverStatistics,
            }, {
                path: '/accidentCollention',
                name: 'accidentCollention',
                component: accidentCollention,
            }, {
                path: '/accidentSupplement',
                name: 'accidentSupplement',
                component: accidentSupplement,
            }, {
                path: '/accidentList',
                name: 'accidentList',
                component: accidentList,
            }, {
                path: '/accidentStatistics',
                name: 'accidentStatistics',
                component: accidentStatistics,
            }, {
                path: '/illegalCollection',
                name: 'illegalCollection',
                component: illegalCollection,
            }, {
                path: '/illegalSupplement',
                name: 'illegalSupplement',
                component: illegalSupplement,
            }, {
                path: '/illegalList',
                name: 'illegalList',
                component: illegalList,
            }, {
                path: '/illegalStatistics',
                name: 'illegalStatistics',
                component: illegalStatistics,
            }, {
                path: '/assessmentList',
                name: 'assessmentList',
                component: assessmentList,
            }, {
                path: '/assessmentStatistics',
                name: 'assessmentStatistics',
                component: assessmentStatistics,
            }, {
                path: '/userManagement',
                name: 'userManagement',
                component: userManagement,
            }, {
                path: '/roleManagement',
                name: 'roleManagement',
                component: roleManagement,
            }, {
                path: '/departmentManagement',
                name: 'departmentManagement',
                component: departmentManagement,
            }, {
                path: '/logManagement',
                name: 'logManagement',
                component: logManagement,
            }, {
                path: '/parameterManagement',
                name: 'parameterManagement',
                component: parameterManagement,
            }
        ]
    },
    {
        name: 'basicInformation',
        path: '/',
        component: basicInformation
    }, {
        name: 'NotFound',
        path: '*',
        component: NotFound  
    }
]


const router = new VueRouter({
    // mode: 'history',
    base: __dirname,
    routes
})


router.beforeEach((to, from, next) => {
    if (to.path !== '/login') {
        router.app.$store.dispatch('getUser').then((res) => {  
            // 此处代码作用同上，是否删除待测
            if (res.code === 1000) {
                next()
            } else {
                next('/login')  
            } 
        })
    } else {
        next()
    }
})

export {
    router,
    VueRouter,
}
