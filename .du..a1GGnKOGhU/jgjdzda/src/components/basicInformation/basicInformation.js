const getData = {
    // 转换数据的data
    transData: (data) => {
        console.log(data.data.content)
        const res = []

        let i = 0
        // 将数据转换成index，title数组
        while (i < data.data.total) {
            res.push({
                index: data.data.content[i].objUuid,
                title: data.data.content[i].objName,
            })
            i++
        }
        return res
    },
}

export {
    getData,
}
