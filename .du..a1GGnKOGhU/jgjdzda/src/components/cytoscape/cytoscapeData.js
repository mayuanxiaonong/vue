// 画布数据

// import { getDate } from './cygetdata'

// getDate.login()
// getDate.getAllTables()

// const testData = getDate.getAllTables()

// const transTableData = (tableData) => {
//     console.log(tableData)
// }

const elementData = [{
    data: {
        id: '605755',
        idInt: 605755,
        name: 'PCNA',
        score: 0.006769776522008331,
        query: true,
        gene: true
    },
    position: {
        x: 481.0169597039117,
        y: 384.8210888234145
    },
    group: 'nodes',
    removed: false,
    selected: false,
    selectable: true,
    locked: false,
    grabbed: false,
    grabbable: true
}]
// console.log(testData)

const styleData = [{
    selector: 'core',
    style: {
        'selection-box-color': '#AAD8FF',
        'selection-box-border-color': '#8BB0D0',
        'selection-box-opacity': '0.5'
    }
}, {
    selector: 'node',
    style: {
        width: 'mapData(score, 0, 0.006769776522008331, 20, 60)',
        height: 'mapData(score, 0, 0.006769776522008331, 20, 60)',
        content: 'data(name)',
        shape: 'rectangle',
        'font-size': '12px',
        'text-valign': 'bottom',
        'text-halign': 'center',
        'background-image': 'assets/text.svg',
        'background-fit': 'cover',
        'background-color': '#fff',
        color: '#fff',
        'overlay-padding': '6px',
        'z-index': '10'
    }
}, {
    selector: 'node[group=\'TABLE\']',
    style: {
        'background-image': 'assets/table.png',
        'background-color': 'rgba(0,0,0,0)'
    }
}, {
    selector: 'node[group=\'FUNCTION\']',
    style: {
        'background-image': 'assets/function.svg',
    }
}, {
    selector: 'node[group=\'INDEX\']',
    style: {
        'background-image': 'assets/catalog.svg',
    }
}, {
    selector: 'node[group=\'PROCEDURE\']',
    style: {
        'background-image': 'assets/process.svg',
    }
}, {
    selector: 'node[group=\'SEQUENCE\']',
    style: {
        'background-image': 'assets/pp_sequence.svg',
    }
}, {
    selector: 'edge',
    style: {
        'curve-style': 'bezier',
        'target-arrow-shape': 'triangle',
        'line-color': '#ffaaaa',
        width: 6,
        'target-arrow-color': '#ffaaaa',
    }
}]

const layout = {
    name: 'cose',
    idealEdgeLength: 100,
    nodeOverlap: 20,
    refresh: 20,
    fit: true,
    padding: 30,
    randomize: false,
    componentSpacing: 100,
    nodeRepulsion: 400000,
    edgeElasticity: 100,
    nestingFactor: 5,
    gravity: 80,
    numIter: 1000,
    initialTemp: 200,
    coolingFactor: 0.95,
    minTemp: 1.0
}

export default {
    elementData, styleData, layout
}
