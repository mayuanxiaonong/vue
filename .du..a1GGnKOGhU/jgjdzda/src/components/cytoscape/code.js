// 初始化画布


import cytoscape from 'cytoscape'
import cytoscapeData from './cytoscapeData'

function initCytoscape(){
    // const elementData = [
    //     { data: { id: 'a', name: 'a' } },
    //     { data: { id: 'b' } },
    //     { data: { id: 'ab', source: 'a', target: 'b' } }
    // ]

    const cy = cytoscape({
        // 获取画布元素
        container: document.getElementById('cy'),
        // 描绘画布元素
        // 布局属性
        layout: cytoscapeData.layout,
        // 样式属性
        style: cytoscapeData.styleData,
        // 元素单元属性
        elements: cytoscapeData.elementData
        

    })
    // console.log(cytoscapeData)

    // 添加节点的单击绑定事件
    // =>语法糖 替代 function function（evt）{} 等价于 （evt） => {}
    cy.on('tap', 'node', (evt) => {
        const node = evt.target
        console.log(node.id())
    })
}

export {
    // 外抛出初始化方法
    initCytoscape,
}
