// import utils from '../common/utils'
// // 引用常量
// import { LOAD_DETAIL } from './mutation-types'


// const { handleVuex } = utils

// const initState = {
//     details: []
// }

// const mutations = {
//     // 使用常量代替Muitation事件类型
//     // 把这些常量放到单独的文件中（mutation-types）
//     [LOAD_DETAIL]: {
//         success: (state, payload) => {
//             const { data } = payload
//             state.details = data.content || []
//         },
//         error: () => {
//         }
//     }
// }

// const actions = {
//     loadDetail: {
//         url: '/dbmApi/ServerStatusInformation',
//         method: 'get',
//         actionType: LOAD_DETAIL
//     }
// }

// export default handleVuex({
//     initState,
//     mutations,
//     actions,
// })

// // state 数据源
// // view 以声明的方式将state映射到视图
// // actions响应在view上的用户输入的变化

// // vuex的核心是store，包含多个state对象和mutation
// // 创建了store，可以通过store.state获取状态对象，通过store.commmit()触发变更
// // Vuex允许我们在store中定义“getter”，可以认为是store中的计算属性
// // Getter会暴露为store.getters对象，可以以属性的形式访问这些值

// // 更改Vuex的store中的状态的唯一办法是提交mutation
// // mutation非常类似事件，每个mutation都有一个字符串的事件类型（type）和一个回调函数（handler）,接受state作为第一个参数
// // 可以向store.commit 传入额外的参数，即荷载(payload)

// // action接受一个与store实例相同的context对象
