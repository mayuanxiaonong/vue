import utils from '../common/utils'
import { BASIC_INFORMATION, PROCBASIC_INFORMATION, TABLE_INFORMATION, RESULT_RUN } from './mutation-types'

const { handleVuex } = utils
const initState = {
    tableList: [],
    /*
    *  字段数组 格式为
    * "tableName": "TB_ML_RPT_PLANT_CULTURE_INFO_C",
        "columnName": "APP_ID",
        "comments": "调查报告编号",
        "colTypeDesc": "VARCHAR2(50)",
        "dataType": "VARCHAR2",
        "dataLength": "50",
        "dataPrecision": null,
        "dataScale": null
    */
    columns: [],
    // 表基本信息
    basic: {
        objCount: '0',
        objSize: '0',
        objName: 'null',
        objType: 'null',
        // total: 0,
    },
    resultlist: {},
    procbasic: {},
    procbasicinformation: {
        name: '',
        describe: '存储过程',
        func: '',
    }
}

const mutations = {
    [BASIC_INFORMATION]: {
        success: (state, payload) => {
            const { data } = payload
            state.columns = data.content
            // state.basic = data.basic
        },
        error: () => {
        }
    },
    [TABLE_INFORMATION]: {
        success: (state, payload) => {
            const { data } = payload
            state.basic.objCount = data.content[0].objCount
            state.basic.objSize = data.content[0].objSize
            state.basic.objName = data.content[0].objName
            state.basic.objType = data.content[0].objType
            // state.basic = data.basic
        },
        error: () => {
        }
    },
    [RESULT_RUN]: {
        success: (state, payload) => {
            const { data } = payload
            state.resultlist = data
        },
        error: () => {
        }
    },
    [PROCBASIC_INFORMATION]: {
        success: (state, payload) => {
            const { data } = payload
            state.procbasic = data
            state.procbasicinformation.name = data[0].name         
        },
        error: () => {
        }
    }
}

const actions = {
    getBasicInformation: {
        url: '/dbmApi/findTableInfo',
        method: 'post',
        actionType: BASIC_INFORMATION,
    },
    getTableInformation: {
        url: '/dbmApi/findOwnersInstance',
        method: 'post',
        actionType: TABLE_INFORMATION,
    },
    getExesqlcontroller: {
        url: '/dbmApi/executeSql',
        method: 'post',
        actionType: RESULT_RUN,
    },
    getProcBasicInformation: {
        url: '/dbmApi/findProcedureInformation',
        method: 'post',
        actionType: PROCBASIC_INFORMATION
    }
}

export default handleVuex({
    state: initState,
    mutations,
    actions,
})
