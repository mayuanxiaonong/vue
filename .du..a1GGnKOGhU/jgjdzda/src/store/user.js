import utils from '../common/utils'
import { LOGIN_USER, REMEMBER_USER, GET_USER, UPDATE_USER } from './mutation-types'

const { Tools, handleVuex } = utils
const initState = {
    user: null,
    rememberMe: Tools.isEmpty(Tools.getCookie('rememberMe')) || Tools.getCookie('rememberMe') === 'true',
}

const mutations = {
    [LOGIN_USER]: {
        success: (state, payload) => {
            const { data } = payload
            state.user = data
        },
        error: () => {
        }
    },
    [REMEMBER_USER](state, rememberMe){
        state.rememberMe = rememberMe
    },
    [GET_USER]: {
        success: (state, payload) => {
            const { data } = payload
            state.user = data
        },
        error: () => {
        }
    },
}

const actions = {
    login: {
        url: '/jgsApi/login',
        method: 'post',
        actionType: LOGIN_USER,
    },
    getUser: {
        url: 'jgsApi/userInfo',
        method: 'get',
        actionType: GET_USER,
    },
    updateUser: {
        url: '/jgsApi/updateInfo',
        method: 'post',
        actionType: UPDATE_USER
    },


    // mock
    // login: {
    //     url: '/mock/login.json',
    //     method: 'get',
    //     actionType: LOGIN_USER,
    // },
    rememberMe: ({ commit }, rememberMe) => {
        commit(REMEMBER_USER, rememberMe)
    }
}

export default handleVuex({
    state: initState,
    mutations,
    actions,
})
