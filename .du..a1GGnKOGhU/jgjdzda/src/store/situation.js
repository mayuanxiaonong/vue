import utils from '../common/utils'
import { ADD_SITUATION, SELECT_SITUATION } from './mutation-types'

const { handleVuex } = utils
const initState = {
    codes: null,
    result: [],
}

const mutations = {
    [ADD_SITUATION]: {
        success: (state, payload) => {
            const { data } = payload
            state.codes = data.code
        },
        error: () => {
        }
    },
    [SELECT_SITUATION]: {
        success: (state, payload) => {
            const { data } = payload
            state.result = data
        },
        error: () => {
        }
    },
}

const actions = {
    addSituation: {
        url: '/dbmApi/addSituation',
        method: 'post',
        actionType: ADD_SITUATION,
    },
    selcetSituation: {
        url: '/dbmApi/selectSituation',
        method: 'post',
        actionType: SELECT_SITUATION,
    } 
}

export default handleVuex({
    state: initState,
    mutations,
    actions,
})
