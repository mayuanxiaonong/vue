import Vue from 'vue'
import axios from 'axios'
import { baseUrl } from 'conf'

/**
 * 注册全局指令v-focus
 */
Vue.directive('focus', {
    // 当元素插入到DOM中
    inserted: (el) => {
        el.focus()
    }
})

const noop = () => {}

/**
 * @deprecated 废弃 czy 2018年5月31日10:43:52，所有请求统一放到store
 * @param {Object} opts 
 */
const ajax = (opts = {
    success: noop,
    error: noop,
}) => {
    if (opts.type === 'post'){
        return axios({
            method: 'post',
            url: `${baseUrl}${opts.url}`,
            data: opts.data || {},
        }).then(res => {
            if (res.data){
                opts.success(res.data)
            } else {
                opts.error(res)
            }
        }).catch(res => {
            opts.error(res)
        })
    }                                                                                           
    return axios({
        method: 'get',
        url: `${baseUrl}${opts.url}`,
    })
}

export {
    ajax,
}
