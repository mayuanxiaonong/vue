const ip = require('ip').address()
const path = require('path')

const ROOT_PATH = path.resolve(__dirname, '../')
const favicon = path.resolve(ROOT_PATH, 'src/assets/favicon.ico')
const applicationContext = ''   // /dbm
const dist = 'dist/'
const distPrefix = `${dist}${applicationContext}`

module.exports = {
    ip: ip.toString(),
    port: 8010,
    favicon: favicon,
    dist: dist,
    distPrefix: distPrefix,
    applicationContext: applicationContext,
    publicPath: '',
    images: 'src/assets/{,*/}*.{gif,jpeg,jpg,png,ico,svg}',
    destImg: `${distPrefix}/assets`,
    fonts: 'src/css/common/fonts/*',
    less: 'src/css/**/*.less',
    lessMain: 'src/css/main.less',
    lessDest: `${distPrefix}/css`,
    destFonts: `${distPrefix}/css/fonts`,
    precommitSrc: 'build/pre-commit',
    precommitDest: '.git/hooks/',
    mainJS: 'app.js',
    mockSrc: path.resolve(ROOT_PATH, 'src/mock/*'),
    mockDest: `${distPrefix}/mock`,
    isPlugin: false,
    // vender: './src/vendor',
    vender: [
        'vue',
        'element-ui',
        'vuex',
        'vue-router'
    ],
    uglifyJsConfig: {
        beautify: false,
        compress: {
            warnings: false,
            drop_debugger: true,
            drop_console: true
        },
        mangle: {
            except: ['$', 'exports', 'require']
        },
        space_colon: false,
        comments: false
    },
    defaultPath: {
        ROOT_PATH,
        APP_PATH: path.resolve(ROOT_PATH, 'src')
    },
    proxy: {
        target: 'http://localhost',
        proxyPort: 8090, //7301
        headers: {
            host: ''
        },
        paths: [`${applicationContext}/jgsApi`]
    },
    v: Date.now(),
    version: '1.0'
}