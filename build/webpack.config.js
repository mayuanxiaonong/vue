const {
    resolve
} = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const url = require('url')
const config = require('./project.config')
const rootDir = './'

const extractCSS = new ExtractTextPlugin('css/[name].css')
// const extractLESS = new ExtractTextPlugin('css/[name].css')

module.exports = {
    watch: true,
    entry: {
        vendor: config.vender,
        app: [
            `webpack-dev-server/client?http://${config.ip}:${config.port}/`,
            'webpack/hot/only-dev-server',
            './src/main.js'
        ]
    },
    output: {
        // path: resolve(rootDir, `${config.dist}`),
        filename: 'scripts/[name].js',
        chunkFilename: '[id].js?[chunkhash]',
        // publicPath: options.dev ? '/assets/' : `${config.publicPath}`,
        sourceMapFilename: 'maps/[file].map?[hash]',
        hotUpdateChunkFilename: 'hot/hot-update.js',
        hotUpdateMainFilename: 'hot/hot-update.json'
    },
    module: {
        rules: [{
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
                transformToRequire: {
                    source: 'src',
                    img: 'src',
                    image: 'xlink:href'
                }
            }
        },
        {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/,
            options: {
                cacheDirectory: true,
                presets: ['env', 'stage-0']
            }
        },
        // {
        //     test: /\.html$/,
        //     use: [{
        //         loader: 'html-loader',
        //         options: {
        //             root: resolve('../', 'src'),
        //             attrs: ['img:src', 'link:href']
        //         }
        //     }]
        // },
        // {
        //     test: /\.less$/,
        //     // use: ['style-loader', 'css-loader', 'less-loader'],
        //     use: extractLESS.extract(['css-loader', 'postcss-loader', 'less-loader'])
        // },
        {
            test: /\.css$/,
            // use: ['style-loader', 'css-loader', 'postcss-loader']
            use: extractCSS.extract(['css-loader', 'postcss-loader'])
        },
        {
            test: /favicon\.png$/,
            use: [{
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]'
                }
            }]
        },
        {
            test: /\.(png|jpg|jpeg|gif|eot|ttf|woff|woff2|svg|svgz)(\?.+)?$/,
            exclude: /favicon\.png$/,
            use: [{
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: 'resources/[name].[ext]'
                }
            }]
        }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        }),
        extractCSS,
        // extractLESS,
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendor', 'runtime'],
            filename: 'scripts/[name].js',
            minChunks: Infinity
        }),
        new webpack.NoEmitOnErrorsPlugin(),
        new FriendlyErrorsPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            favicon: config.favicon,
            title: '河北省交管局档案系统',
            description: 'Vue demo',
            applicationContext: config.applicationContext,
            inject: 'body',
            filename: 'index.html',
            chunks: ['runtime', 'vendor', 'app'],
            chunksSortMode: 'auto',
            minify: {
                removeComments: true
            },
            cache: false
        })
    ],
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            '~': resolve(rootDir, './src'),
            root: resolve(rootDir, './src'),
            css: resolve(rootDir, './src/css'),
            common: resolve(rootDir, './src/common'),
            conf: resolve(rootDir, './src/common/conf'),
        },
        modules: [
            config.defaultPath.APP_PATH,
            'node_modules'
        ]
    },
    devServer: {
        contentBase: './dist',
        publicPath: config.publicPath,
        clientLogLevel: 'none',
        host: config.ip,
        port: config.port,
        hot: true,
        inline: true,
        compress: true,
        stats: {
            colors: true,
            errors: true,
            warnings: true,
            modules: false,
            chunks: false
        },
        proxy: (function(){
            const obj = {}
            const target = `${config.proxy.target}:${config.proxy.proxyPort}`
            config.proxy.paths.forEach((apiPath) => {
                obj[apiPath] = target
            })
        }),
        watchOptions: {
            pool: false
        },
        historyApiFallback: true
    },
    // devtool: '#eval-source-map'
    devtool: 'cheap-module-eval-source-map'
}
