/**
 * 工具类
 * czy
 * 2018年5月12日10:15:34
 */
export default class Tools{
    static getHash(){
        return window.location.hash
    }

    static getPathname() {
        return window.location.pathname
    }
    /**
     * 获取字符串长度，区分中英文
     * @param {string} str 
     */
    static getStrLen(str){
        if (!str){
            return 0
        }
        const aStr = `${str}`
        let len = 0
        for (let i = 0; i < aStr.length; i++){
            if (aStr.charCodeAt(i) > 127 || aStr.charCodeAt(i) === 94){
                len += 2
            } else {
                len++
            }
        }
        return len
    }

    static setCookie(name, value, day){
        const Days = day || 30
        const exp = new Date()
        exp.setTime(exp.getTime() + (Days * 24 * 60 * 60 * 1000))
        document.cookie = `${name}=${escape(value)};expires=${exp.toGMTString()}`
    }

    static getCookie(name){
        const reg = new RegExp(`(^| )${name}=([^;]*)(;|$)`)
        const arr = document.cookie.match(reg)
        if (arr){
            return unescape(arr[2])
        }
        return null
    }

    static delCookie(name){
        const exp = new Date()
        exp.setTime(exp.getTime() - 1)
        const cval = this.getCookie(name)
        if (cval !== null){
            document.cookie = `${name}=${cval};expires=${exp.toGMTString()}`
        }
    }

    static isEmpty(param){
        if (typeof param === 'object'){
            if (param === null || (Object.prototype.toString.call(param) === '[object Array]' && param.length === 0) ||
            (Object.hasOwnProperty.call(param, 'length') && param.length === 0)){
                return true
            }
            for (const name in param){
                if (Object.hasOwnProperty.call(param, name)){
                    return false
                }
            }
            return true
        }
        return (typeof param === 'undefined' || (typeof param === 'string' && param === '') || (typeof param === 'number' && window.isNaN(param)))
    }

    static getType(obj){
        const toString = Object.prototype.toString
        const map = {
            '[object Boolean]': 'boolean',
            '[object Number]': 'number',
            '[object String]': 'string',
            '[object Function]': 'function',
            '[object Array]': 'array',
            '[object Date]': 'date',
            '[object RegExp]': 'regExp',
            '[object Undefined]': 'undefined',
            '[object Null]': 'null',
            '[object Object]': 'object',
        }
        if (obj instanceof Element){
            return 'element'
        }
        return map[toString.call(obj)]
    }
}
