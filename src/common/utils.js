import moment from 'moment'
import Element from 'element-ui'
import { mapActions, mapState } from 'vuex'
import axios from 'axios'
import cytoscape from 'cytoscape'
import config from 'conf'
import Tools from './Tools'
import MITools from './MITools'

moment.locale('zh-cn')
const { baseUrl } = config

const instance = axios.create({
    method: 'get',
    baseURL: baseUrl,
    timeout: 60 * 1000,
    responseType: 'json'
})

const handleWithParameter = function (url, {
    method = 'GET',
    contentType = 'application/json; charset=UTF-8',
    params = {},
    data = {}
}){
    const { headers } = instance.defaults
    instance.defaults.headers = { ...headers, 'Content-Type': contentType }
    
    let urlNew = url
    const paramNew = { ...params }
    /*eslint-disable*/
    for (const key in params){
        const reg = new RegExp(`:${key}`, 'g')
        if (Object.hasOwnProperty.call(params, key) && reg.test(urlNew)){
            urlNew = urlNew.replace(reg, params[key])
            delete paramsNew[key]
        }
    }

    urlNew = `${baseUrl}${urlNew}`

    switch (method.toLowerCase()){
        case 'get':
            return instance.get(urlNew, { params: paramNew })
        case 'post':
            return instance.post(urlNew, JSON.stringify(data))
        case 'delete':
            return instance.delete(urlNew, { params: paramNew })
        case 'put':
            return instance.put(urlNew, JSON.stringify(data))
        default: {
            const res = {
                then: resolve => resolve({
                    code: '3000',
                    message: 'method方式错误'
                })
            }
            return Promise.resolve(res)
        }
    }
}
/**
 * 生成action
 * @param {Object} actionMap 
 */
const createActions = function(actionMap) {
    const eventNames = Object.keys(actionMap)
    const fnMap = {}
    eventNames.forEach((eventName) => {
        const configOrFn = actionMap[eventName]
        if (typeof configOrFn !== 'function'){
            const config = { method: 'GET', ...configOrFn }
            fnMap[eventName] = ({ commit }, settings = {}) => {
                return handleWithParameter(
                    config.url,
                    {
                        ...settings,
                        ...config
                    }
                ).then((res) => {
                    const { data } = res
                    commit(`${configOrFn.actionType}_SUCCESS`, data)
                    return data
                }).catch((error) => {
                    commit(`${configOrFn.actionType}_ERROR`, { error })
                    return error
                })
            }
        } else {
            fnMap[eventName] = configOrFn
        }
    })
    return fnMap
}
/**
 * 扩展mutation
 * @param {Object} mutationMap 
 */
const createMutations = function(mutationMap = {}){
    const result = {}
    Object.keys(mutationMap).forEach((actionType) => {
        const fnOrObject = mutationMap[actionType]
        if (fnOrObject && typeof fnOrObject !== 'function'){
            Object.keys(fnOrObject).forEach((suffixAction) => {
                result[`${actionType}_${suffixAction.toUpperCase()}`] = fnOrObject[suffixAction]
            })
        } else {
            result[actionType] = fnOrObject
        }
    })
    return result
}

const handleVuex = function({
    state,
    mutations,
    actions,
}){
    return {
        state,
        mutations: createMutations(mutations),
        actions: createActions(actions),
    }
}

export default {
    Tools,
    moment,
    MITools,
    noop: () => {},
    Element,
    mapActions,
    mapState,
    createActions,
    handleVuex,
    cytoscape,
}
