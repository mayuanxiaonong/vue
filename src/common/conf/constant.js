/**
 * 常量配置
 */
export const statusCode = {
    OK: 1000,
    ERROR: 2000,
    FAIL: 3000,
    NOT_FOUND_USER: 4001,
    BAD_PWD: 3102,
}
