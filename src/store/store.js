import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
import cydata from './cydata'
import basicInformation from './basicInformation'
import obj from './obj'
import situation from './situation'
import runLog from './runLog'
import uploadImg from './uploadImg'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        user,
        cydata,
        basicInformation,
        obj,
        situation,
        runLog,
        uploadImg,
    }
})
