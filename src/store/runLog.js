import utils from '../common/utils'
import { SELECT_RUNLOG } from './mutation-types'

const { handleVuex } = utils
const initState = {
    list: [],
}

const mutations = {
    [SELECT_RUNLOG]: {
        success: (state, payload) => {
            const { data } = payload
            state.list = data.content
        },
        error: () => {
        }
    },
}

const actions = {
    selectRunLog: {
        url: '/dbmApi/findRunLogMonitor',
        method: 'post',
        actionType: SELECT_RUNLOG,
    }
}

export default handleVuex({
    state: initState,
    mutations,
    actions,
})
