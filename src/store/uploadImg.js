import utils from '../common/utils'
import { UPLOAD_IMG } from './mutation-types'

const { handleVuex } = utils
const initState = {
    result: '',
}

const mutations = {
    [UPLOAD_IMG]: {
        success: (state, payload) => {
            const { data } = payload
            state.result = data
        },
        error: () => {
        }
    },
}

const actions = {
    uploadImg: {
        url: '/jgsApi/uploadImg',
        method: 'post',
        actionType: UPLOAD_IMG,
    }
}

export default handleVuex({
    state: initState,
    mutations,
    actions,
})
