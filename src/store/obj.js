import utils from '../common/utils'
import { GET_LIST_OBJECTLIST, GET_EXAMPLES, SET_OBJECTID_OBJECT, GET_OBJECT_NUMBER, LOAD_DETAIL } from './mutation-types'

const { handleVuex } = utils
const initState = {
    list: [],
    array: [],
    objectId: '',
    objectNumber: [],
    details: []
}

const mutations = {
    [GET_LIST_OBJECTLIST]: {
        success: (state, payload) => {
            const { data } = payload
            state.list = data.content || []
        },
        error: () => {
        }
    },
    [GET_EXAMPLES]: {
        success: (state, payload) => {
            const { data } = payload
            state.array = data
        },
        error: () => {
        }
    },
    [SET_OBJECTID_OBJECT](state, payload){
        state.objectId = payload
    },
    [GET_OBJECT_NUMBER]: {
        success: (state, payload) => {
            const { data } = payload
            state.objectNumber = data
        },
        error: () => {
        }
    },
    [LOAD_DETAIL]: {
        success: (state, payload) => {
            const { data } = payload
            state.details = data.content || []
        },
        error: () => {
        }
    }
}

const actions = {
    getObjectList: {
        url: '/dbmApi/findOwnersInstance',
        method: 'post',
        actionType: GET_LIST_OBJECTLIST,
    },
    getExamples: {
        url: '/dbmApi/findOwnerList',
        method: 'get',
        actionType: GET_EXAMPLES
    },
    setObjectId: ({ commit }, objectId) => {
        commit(SET_OBJECTID_OBJECT, objectId)
    },
    getObjectNumber: {
        url: '/dbmApi/findOwnerTypeCnt',
        method: 'post',
        actionType: GET_OBJECT_NUMBER
    },
    loadDetail: {
        url: '/dbmApi/ServerStatusInformation',
        method: 'get',
        actionType: LOAD_DETAIL
    }
}

export default handleVuex({
    state: initState,
    mutations,
    actions,
})
