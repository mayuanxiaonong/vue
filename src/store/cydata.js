import utils from '../common/utils'
import { CY_GETDATA, CY_FINDRELATION } from './mutation-types'


const { handleVuex } = utils

const initState = {
    user: {
        id: '',
        name: ''
    },
    // rememberMe: Tools.isEmpty(Tools.getCookie('rememberMe')) || Tools.getCookie('rememberMe') === 'true',
}

const mutations = {
    [CY_GETDATA]: {
        success: () => {
            // console.log('cydata输出')
            // console.log(state)
        },
        error: () => {
        }
    },
    [CY_FINDRELATION]: {
        success: () => {
            // 输出成功的函数
        },
        error: () => {
            // 输出失败的处理方法
        }
    }
}


const actions = {
    // 声明获取所有数据的URL地址和方法
    // i/findAllInstance
    cyGetAllData: {
        // url: '/dbmApi/findOwnersInstanceByType',
        url: '/dbmApi/findInstanceRelation',
        method: 'post',
        actionType: CY_GETDATA,
    },
    cyFindRelation: {
        url: '/dbmApi/findInstanceRelation',
        method: 'post',
        actionType: CY_FINDRELATION
    }
}

export default handleVuex({
    initState,
    mutations,
    actions,
})
