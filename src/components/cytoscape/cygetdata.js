import axios from 'axios'


function getInstaceDate(){
    console.log('this is get date')
    const params = {
        criteria: { owner: 'pdata', objectType: 'table', objectName: 'tb_ml_loan_mid_base_1805' },
        fromSize: 'string',
        returnSize: 'string' 
    }
    const url = '/dbmApi/findAllInstance'
    axios.post(url, params).then((response) => {
        console.log('这是相应' + response)
        console.log(response)
    })
}


const getData = {
    // 转换数据的data
    transData: (content = []) => {
        const result = []
        // 将数据添加到Node中
        content.forEach(item => {
            // 传入三个数据，分别添加到系统
            // 传入源数据Node
            result.push({
                data: {
                    id: item.previousObjUuid,
                    name: item.previousObjName,
                    score: 0.006769776522008331,
                    query: true,
                    gene: true,
                    group: item.previousObjType
                },
                position: {
                    x: Math.random() * 1500,
                    y: Math.random() * 700
                },
                group: 'nodes',
                removed: false,
                selected: false,
                selectable: true,
                locked: false,
                grabbed: false,
                grabbable: true
            })
            // 传入目标数据Node
            result.push({
                data: {
                    id: item.objUuid,
                    name: item.objName,
                    score: 0.006769776522008331,
                    query: true,
                    gene: true,
                    group: item.objType
                },
                position: {
                    x: Math.random() * 2000,
                    y: Math.random() * 700
                },
                group: 'nodes',
                removed: false,
                selected: false,
                selectable: true,
                locked: false,
                grabbed: false,
                grabbable: true
            })
            // 传入关系连接
            result.push({
                data: {
                    source: item.previousObjUuid,
                    target: item.objUuid,
                    group: item.relationId,
                    id: item.previousObjUuid + item.objUuid
                },
                position: { },
                group: 'edges',
                removed: false,
                selected: false,
                selectable: true,
                locked: false,
                grabbed: false,
                grabbable: true
            })
        })
        
        return result
    },
    login: () => {
        console.log('登录了')
        this.login({
            data: { 
                criteria: { userId: '00158', pwd: '00158' }, 
                fromSize: '0', 
                returnSize: '10'  
            }
        }).then((res) => {
            console.log(res)
        }) 
    }
}

export {
    getInstaceDate,
    getData,
}
