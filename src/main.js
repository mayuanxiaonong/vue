import 'babel-polyfill'
import Vue from 'vue'
import ElementUI from 'element-ui'
import lang from 'element-ui/lib/locale/lang/zh-CN'
import locale from 'element-ui/lib/locale'
// gulp处理
// import 'css/main.less'
import store from './store/store'
import { VueRouter, router } from './routes'
import Main from './components/main.vue'
import VueCropper from 'vue-cropper'
import echarts from 'echarts'

Vue.prototype.$echarts = echarts 

Vue.use(VueRouter)

locale.use(lang)

Vue.use(ElementUI)
Vue.use(VueCropper)

// el参数 DOM元素中的id

new Vue({
    el: '#app',   
    router,
    store,
    render: h => h(Main),
    
})
